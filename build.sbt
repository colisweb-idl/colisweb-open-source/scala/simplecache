import sbt.Keys.crossScalaVersions
import DependenciesScopesHandler.*
import PublishSettings.*
import Dependencies.*
import CacheSettings.*
import RunBeforeSettings.*
import CISettings.notOnCI
import org.typelevel.scalacoptions.ScalacOptions

lazy val latestScala213         = "2.13.16"
lazy val scala213versions       = List()
lazy val supportedScalaVersions = latestScala213 :: scala213versions

Global / onChangedBuildSource := ReloadOnSourceChanges

inThisBuild {
  List(
    organization       := "com.colisweb",
    scalaVersion       := latestScala213,
    crossScalaVersions := supportedScalaVersions,
    scalafmtOnCompile  := notOnCI,
    scalafmtCheck      := notOnCI,
    scalafmtSbtCheck   := notOnCI,
    Test / fork        := true,
    localCacheSettings
  )
}

lazy val simplecache = Project(id = "simplecache", base = file("."))
  .aggregate(core, caches, wrappers)
  .settings(noPublishSettings)

lazy val core = Project(id = "simplecache-core", base = file("core"))
  .settings(Test / tpolecatExcludeOptions += ScalacOptions.warnNonUnitStatement)
  .settings(libraryDependencies ++= compileDependencies(slf4jApi))
  .settings(libraryDependencies ++= testDependencies(scalatest))

lazy val wrappers = Project(id = "wrappers", base = file("wrappers"))
  .aggregate(errorWrapper, zioWrapper, catsWrapper)
  .settings(noPublishSettings)

lazy val errorWrapper = Project(id = "simplecache-wrapper-error", base = file("wrappers/error"))

lazy val zioWrapper = Project(id = "simplecache-wrapper-zio", base = file("wrappers/zio"))
  .dependsOn(errorWrapper, core % "test->test;compile->compile")
  .settings(libraryDependencies ++= compileDependencies(zio))

lazy val catsWrapper = Project(id = "simplecache-wrapper-cats", base = file("wrappers/cats"))
  .dependsOn(errorWrapper, core % "test->test;compile->compile")
  .settings(libraryDependencies ++= compileDependencies(cats, catsEffects))

lazy val caches = Project(id = "caches", base = file("caches"))
  .aggregate(memoryCache, memoryCacheGuava, redisCache, redisCacheCirce)
  .settings(noPublishSettings)

lazy val memoryCache = Project(id = "simplecache-memory", base = file("caches/memory"))
  .settings(Test / tpolecatExcludeOptions += ScalacOptions.warnNonUnitStatement)
  .dependsOn(core % "test->test;compile->compile", zioWrapper % "test->test", catsWrapper % "test->test")

lazy val memoryCacheGuava = Project(id = "simplecache-memory-guava", base = file("caches/memory-guava"))
  .dependsOn(core % "test->test;compile->compile", zioWrapper % "test->test", catsWrapper % "test->test")
  .settings(libraryDependencies ++= compileDependencies(cacheGuava))

lazy val redisCache = Project(id = "simplecache-redis", base = file("caches/redis"))
  .dependsOn(core % "test->test;compile->compile", zioWrapper % "test->test", catsWrapper % "test->test")
  .settings(libraryDependencies ++= compileDependencies(jedisClient))
  .settings(libraryDependencies ++= testDependencies(pureconfigGeneric))
  .settings(startRedisBefore(Test / test))
  .settings(redisDockerSettings("caches/redis/src/test/resources/application.conf", "simplecache"))

lazy val redisCacheCirce = Project(id = "simplecache-redis-circe", base = file("caches/redis-circe"))
  .dependsOn(redisCache % "test->test;compile->compile")
  .settings(Test / tpolecatExcludeOptions += ScalacOptions.warnNonUnitStatement)
  .settings(libraryDependencies ++= compileDependencies(circeCore, circeJawn))
  .settings(libraryDependencies ++= testDependencies(circeGenericExtras))
