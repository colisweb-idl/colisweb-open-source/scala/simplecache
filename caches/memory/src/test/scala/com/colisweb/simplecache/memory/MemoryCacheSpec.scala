package com.colisweb.simplecache.memory

import com.colisweb.simplecache.core.CacheSpecHelper

class MemoryCacheSpec extends CacheSpecHelper {
  def cache = new MemoryCache[String, String]()
  "MemoryCache" should { behave like aValidCache("one" -> "two", "two" -> "four")(cache) }
}
