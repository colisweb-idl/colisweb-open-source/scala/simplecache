package com.colisweb.simplecache.memory

import cats.effect.IO
import com.colisweb.simplecache.wrapper.cats.{CatsCache, CatsCacheSpecHelper}

class CatsMemoryCacheSpec extends CatsCacheSpecHelper {
  def cache                                    = new MemoryCache[String, String]()
  val catsCache: CatsCache[IO, String, String] = CatsCache.async(cache)
  "MemoryCache" should { behave like aValidCatsCache("one" -> "two", "two" -> "four")(catsCache) }
}
