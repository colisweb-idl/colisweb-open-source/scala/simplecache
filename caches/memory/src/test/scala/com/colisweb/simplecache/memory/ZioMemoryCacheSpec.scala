package com.colisweb.simplecache.memory

import com.colisweb.simplecache.wrapper.zio.{ZioCache, ZioCacheSpecHelper}

class ZioMemoryCacheSpec extends ZioCacheSpecHelper {
  def cache                              = new MemoryCache[String, String]()
  val zioCache: ZioCache[String, String] = ZioCache(cache)
  "MemoryCache" should { behave like aValidZioCache("one" -> "two", "two" -> "four")(zioCache) }
}
