package com.colisweb.simplecache.memory

import com.colisweb.simplecache.core.Cache

import scala.collection.concurrent.TrieMap

class MemoryCache[Key, Value] extends Cache[Key, Value] {
  private val localCache: TrieMap[Key, Value] = TrieMap.empty[Key, Value]

  override def get(key: Key): Option[Value] = localCache.get(key)

  override def update(key: Key, value: Value): Unit = {
    localCache += key -> value
    ()
  }

  override def remove(key: Key): Unit = {
    localCache.remove(key): Unit
    ()
  }

  override def clear(): Unit = localCache.clear()

}
