package com.colisweb.simplecache.memory.guava

import com.colisweb.simplecache.core.{Cache, CacheSpecHelper}
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.concurrent.PatienceConfiguration.{Interval, Timeout}
import org.scalatest.matchers.should.Matchers._
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

class GuavaCacheSpec extends CacheSpecHelper {
  def cacheWithTtl    = new GuavaCache[String, String](Some(Duration(10L, TimeUnit.SECONDS)))
  def cacheWithoutTtl = new GuavaCache[String, String](None)

  def beValidCache(c: Cache[String, String]): Unit = behave like aValidCache("one" -> "two", "two" -> "four")(c)
  "GuavaCacheSpec without a ttl" should beValidCache(cacheWithoutTtl)
  "GuavaCacheSpec with a ttl" should beValidCache(cacheWithTtl)
  it should {
    "remove the value after the ttl has passed" in {
      val timedCached = cacheWithTtl
      timedCached.update("test", "test")
      eventually(Timeout(20.seconds), Interval(5.seconds))(
        timedCached.get("test") should not be defined
      )
    }
  }
}
