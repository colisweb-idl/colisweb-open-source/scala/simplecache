package com.colisweb.simplecache.memory.guava

import cats.effect.IO
import com.colisweb.simplecache.core.Cache
import com.colisweb.simplecache.wrapper.cats.{CatsCache, CatsCacheSpecHelper}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

class CatsGuavaCacheSpec extends CatsCacheSpecHelper {
  def cacheWithTtl    = new GuavaCache[String, String](Some(Duration(30L, TimeUnit.SECONDS)))
  def cacheWithoutTtl = new GuavaCache[String, String](None)

  def beValidCache(c: Cache[String, String]): Unit = {
    val catsCache: CatsCache[IO, String, String] = CatsCache.async(c)
    behave like aValidCatsCache("one" -> "two", "two" -> "four")(catsCache)
  }
  "GuavaCacheSpec with a ttl" should beValidCache(cacheWithTtl)
  "GuavaCacheSpec without a ttl" should beValidCache(cacheWithoutTtl)
}
