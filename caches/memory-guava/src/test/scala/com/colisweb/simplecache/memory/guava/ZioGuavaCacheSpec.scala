package com.colisweb.simplecache.memory.guava

import com.colisweb.simplecache.core.Cache
import com.colisweb.simplecache.wrapper.zio.{ZioCache, ZioCacheSpecHelper}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

class ZioGuavaCacheSpec extends ZioCacheSpecHelper {
  def cacheWithTtl    = new GuavaCache[String, String](Some(Duration(30L, TimeUnit.SECONDS)))
  def cacheWithoutTtl = new GuavaCache[String, String](None)

  def beValidCache(c: Cache[String, String]): Unit = {
    val zioCache = ZioCache(c)
    behave like aValidZioCache("one" -> "two", "two" -> "four")(zioCache)
  }
  "GuavaCacheSpec with a ttl" should beValidCache(cacheWithTtl)
  "GuavaCacheSpec without a ttl" should beValidCache(cacheWithoutTtl)
}
