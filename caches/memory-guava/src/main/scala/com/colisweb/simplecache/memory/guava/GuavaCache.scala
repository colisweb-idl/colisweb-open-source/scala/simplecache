package com.colisweb.simplecache.memory.guava

import com.colisweb.simplecache.core.Cache
import com.google.common.cache
import com.google.common.cache.CacheBuilder

import scala.concurrent.duration.{Duration, FiniteDuration}

class GuavaCache[Key, Value](ttl: Option[FiniteDuration]) extends Cache[Key, Value] {

  private val guava: cache.Cache[Key, Value] =
    ttl match {
      case Some(duration: Duration) =>
        CacheBuilder.newBuilder().expireAfterWrite(duration.length, duration.unit).build[Key, Value]()
      case None                     => CacheBuilder.newBuilder().build[Key, Value]()
    }

  override def get(key: Key): Option[Value]         = Option(guava.getIfPresent(key))
  override def update(key: Key, value: Value): Unit = guava.put(key, value)
  override def remove(key: Key): Unit               = guava.invalidate(key)
  override def clear(): Unit                        = guava.invalidateAll()
}
