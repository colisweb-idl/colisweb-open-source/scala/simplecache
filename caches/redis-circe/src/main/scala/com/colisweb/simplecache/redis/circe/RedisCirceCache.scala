package com.colisweb.simplecache.redis.circe

import com.colisweb.simplecache.core.Cache
import com.colisweb.simplecache.redis.RedisCache
import com.colisweb.simplecache.redis.codec._
import io.circe.jawn.decodeByteArray
import redis.clients.jedis.JedisPool

import scala.concurrent.duration.FiniteDuration
import scala.util.Try

class RedisCirceCache[Key, Value](pool: JedisPool, ttl: Option[FiniteDuration] = None)(
    keyEncoder: Encoder[Key] = AnyEncoder(),
    valueCodec: io.circe.Codec[Value]
) extends Cache[Key, Value] {

  private val cacheCodec: Codec[Value] =
    new Codec[Value] {
      override def decode(array: Array[Byte]): Try[Value] = decodeByteArray[Value](array)(valueCodec).toTry
      override def encode(value: Value): Array[Byte]      = valueCodec(value).noSpaces.getBytes(defaultCharset)
    }

  private val cache: Cache[Key, Value] = new RedisCache[Key, Value](pool, ttl)(keyEncoder, cacheCodec)

  override def get(key: Key): Option[Value]         = cache.get(key)
  override def update(key: Key, value: Value): Unit = cache.update(key, value)
  override def remove(key: Key): Unit               = cache.remove(key)
  override def clear(): Unit                        = cache.clear()
}
