package com.colisweb.simplecache.redis.circe

import com.colisweb.simplecache.core.{Cache, CacheSpecHelper}
import com.colisweb.simplecache.redis.RedisCacheSpec.unsafeLoad
import com.colisweb.simplecache.redis.circe.RedisCirceCacheSpec.{Config, configCodec}
import com.colisweb.simplecache.redis.codec.AnyEncoder
import com.colisweb.simplecache.redis.RedisConfiguration
import io.circe.{Codec, Decoder, Encoder}
import redis.clients.jedis.JedisPool

import scala.concurrent.duration.{DurationInt, FiniteDuration}

class RedisCirceCacheSpec extends CacheSpecHelper {
  val redisConfiguration: RedisConfiguration = unsafeLoad()
  val pool: JedisPool                        = RedisConfiguration.pool(redisConfiguration)

  val ttl: FiniteDuration = 1.minute

  "RedisCirceCache for String" should {

    def stringCache(ttl: Option[FiniteDuration]): RedisCirceCache[String, String] =
      new RedisCirceCache[String, String](pool, ttl)(
        keyEncoder = AnyEncoder(),
        Codec.from(Decoder.decodeString, Encoder.encodeString)
      )

    def beValidCache(c: Cache[String, String]): Unit =
      behave like aValidCache("ala" -> "makota", "the" -> "thing")(c)

    "without a ttl" should beValidCache(stringCache(None))
    "with a ttl" should beValidCache(stringCache(Some(ttl)))
  }

  "RedisCirceCache for a case class" should {

    def classCache(ttl: Option[FiniteDuration]): RedisCirceCache[String, Config] =
      new RedisCirceCache[String, Config](pool, ttl)(keyEncoder = AnyEncoder(), configCodec)

    val remoteConfig = Config(port = 8080, host = "http://colisweb.com")
    val localConfig  = Config(port = 8080, host = "0.0.0.0")

    def beValidCache(c: Cache[String, Config]): Unit =
      behave like aValidCache("ala" -> remoteConfig, "the" -> localConfig)(c)
    "without a ttl" should beValidCache(classCache(None))
    "with a ttl" should beValidCache(classCache(Some(ttl)))
  }

}

object RedisCirceCacheSpec {
  import io.circe.generic.extras.Configuration
  import io.circe.generic.extras.defaults._
  import io.circe.generic.extras.semiauto.deriveConfiguredCodec

  implicitly[Configuration]

  final case class Config(port: Int, host: String)
  val configCodec: io.circe.Codec[Config] = deriveConfiguredCodec
}
