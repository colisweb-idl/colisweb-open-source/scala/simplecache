package com.colisweb.simplecache.redis

import com.colisweb.simplecache.core.{Cache, CacheSpecHelper}
import com.colisweb.simplecache.redis.RedisCacheSpec.unsafeLoad
import com.colisweb.simplecache.redis.codec._
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.concurrent.PatienceConfiguration.{Interval, Timeout}
import org.scalatest.matchers.should.Matchers._
import pureconfig.ConfigSource
import pureconfig.generic.auto._
import redis.clients.jedis.JedisPool

import scala.concurrent.duration.{DurationInt, FiniteDuration}

class RedisCacheSpec extends CacheSpecHelper {
  private val redisConfiguration: RedisConfiguration = unsafeLoad()
  private val pool: JedisPool                        = RedisConfiguration.pool(redisConfiguration)

  private val ttl: FiniteDuration                  = 10.second
  def beValidCache(c: Cache[String, String]): Unit = behave like aValidCache("ala" -> "makota", "the" -> "thing")(c)

  def cache(ttl: Option[FiniteDuration]): RedisCache[String, String] =
    new RedisCache[String, String](pool, ttl)(keyEncoder = AnyEncoder(), valueCodec = StringCodec())

  "RedisCache without a ttl" should beValidCache(cache(None))

  "RedisCache with a ttl" should beValidCache(cache(Some(ttl)))
  it should {
    "remove the value after the ttl has passed" in {
      val timedCached = cache(Some(ttl))
      timedCached.update("test", "test")
      eventually(Timeout(20.seconds), Interval(5.seconds))(
        timedCached.get("test") should not be defined
      )
    }
  }
}

object RedisCacheSpec {
  def unsafeLoad(): RedisConfiguration = ConfigSource.default.at("cache").loadOrThrow[RedisConfiguration]
}
