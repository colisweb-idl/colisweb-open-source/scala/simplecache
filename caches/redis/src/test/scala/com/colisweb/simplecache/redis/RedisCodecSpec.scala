package com.colisweb.simplecache.redis

import com.colisweb.simplecache.redis.codec._
import org.scalatest.Assertion
import org.scalatest.matchers.should.Matchers._
import org.scalatest.wordspec.AnyWordSpec

class RedisCodecSpec extends AnyWordSpec {
  "RedisCodec" should {
    "be symmetrical for Int" in { testSymmetry(5679)(IntCodec()) }
    "be symmetrical for Long" in { testSymmetry(5698779L)(LongCodec()) }
    "be symmetrical for Double" in { testSymmetry(56.79)(DoubleCodec()) }
    "be symmetrical for String" in { testSymmetry("5679")(StringCodec()) }
    "AnyEncoder" should {
      final case class EncoderTest(one: Int, two: String)
      val encoder = AnyEncoder()

      "encode correctly for a simple value" in { encoder.anyToString(123) shouldEqual "123" }
      "encode correctly for a case class" in { encoder.anyToString(EncoderTest(1, "one")) shouldEqual "1:one" }
      "encode correctly for a List of values" in { encoder.anyToString(List(1, 2)) shouldEqual "1:List(2)" }
      "encode correctly for a List of case class" in {
        val input = List(EncoderTest(1, "one"), EncoderTest(2, "two"))
        encoder.anyToString(input) shouldEqual "EncoderTest(1,one):List(EncoderTest(2,two))"
      }
    }
  }

  private def testSymmetry[V](value: V)(codec: Codec[V]): Assertion = {
    val encoded = codec.encode(value)
    val decoded = codec.decode(encoded).get
    value shouldEqual decoded
  }
}
