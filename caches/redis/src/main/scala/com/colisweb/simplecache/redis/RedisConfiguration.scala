package com.colisweb.simplecache.redis

import org.apache.commons.pool2.impl.GenericObjectPoolConfig
import redis.clients.jedis.{Jedis, JedisPool, Protocol}

final case class RedisConfiguration(
    host: String = Protocol.DEFAULT_HOST,
    port: Int = Protocol.DEFAULT_PORT,
    timeout: Int = Protocol.DEFAULT_TIMEOUT,
    database: Int = Protocol.DEFAULT_DATABASE,
    password: Option[String] = None
)

object RedisConfiguration {

  def pool(configuration: RedisConfiguration): JedisPool = {
    pool(
      configuration.host,
      configuration.port,
      configuration.timeout,
      configuration.database,
      configuration.password
    )
  }

  def pool(host: String, port: Int, timeout: Int, database: Int, password: Option[String]): JedisPool = {
    val poolConfig: GenericObjectPoolConfig[Jedis] = new GenericObjectPoolConfig()
    new JedisPool(
      poolConfig,
      host,
      port,
      timeout,
      password.orNull,
      database
    )
  }

}
