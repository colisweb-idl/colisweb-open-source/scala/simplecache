import sbt.*

object Versions {
  final val cats               = "2.13.0"
  final val catsEffects        = "3.5.7"
  final val cache              = "33.4.0-jre"
  final val circe              = "0.14.10"
  final val circeGenericExtras = "0.14.4"
  final val jedis              = "5.2.0"
  final val pureconfig         = "0.17.8"
  final val scalatest          = "3.2.19"
  final val slf4j              = "2.0.17"
  final val zio                = "2.1.16"
}

object Dependencies {
  final val cacheGuava         = "com.google.guava"       % "guava"                % Versions.cache
  final val cats               = "org.typelevel"         %% "cats-core"            % Versions.cats
  final val catsEffects        = "org.typelevel"         %% "cats-effect"          % Versions.catsEffects
  final val circeCore          = "io.circe"              %% "circe-core"           % Versions.circe
  final val circeGenericExtras = "io.circe"              %% "circe-generic-extras" % Versions.circeGenericExtras
  final val circeJawn          = "io.circe"              %% "circe-jawn"           % Versions.circe
  final val jedisClient        = "redis.clients"          % "jedis"                % Versions.jedis
  final val pureconfigGeneric  = "com.github.pureconfig" %% "pureconfig-generic"   % Versions.pureconfig
  final val scalatest          = "org.scalatest"         %% "scalatest"            % Versions.scalatest
  final val slf4jApi           = "org.slf4j"              % "slf4j-api"            % Versions.slf4j
  final val zio                = "dev.zio"               %% "zio"                  % Versions.zio

}
