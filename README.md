
Simplecache is a small and minimalist caching library

## Modules

### Core
The default API of Simplecache. The signature is loosely based on `scala.collection.Map`

The `RichCache` implicit class is here to avoid cluttering the `Cache`.

`RichCache#getOrElseUpdate` prioritise returning a value. If the cache is unavailable, errors will be ignored

### Caches
#### Memory
A `TrieMap` implementation of `Cache`

#### Memory Guava
An cache using google guava in order to manage a ttl

#### Redis
A redis-based `Cache` using Jedis

#### Redis Circe
A cache leveraging the redis implementation with Circe for serialization

### Wrappers
Wrappers are made to manage side effect using ZIO or cats
Like in core, the `getOrElseUpdate` function prioritise returning a value over managing caching errors.

