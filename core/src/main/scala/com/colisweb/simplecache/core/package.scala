package com.colisweb.simplecache

import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}

package object core {
  implicit class RichCache[Key, Value](cache: Cache[Key, Value]) {
    private val logger = LoggerFactory.getLogger(getClass)

    def getOrElseUpdate(key: Key, task: => Value): Value = {
      tryGet(key) match {
        case Some(value) => value
        case None        => tryUpdate(key, task)
      }
    }

    private def tryGet(key: Key): Option[Value] =
      Try(cache.get(key)) match {
        case Success(value)     => value
        case Failure(exception) =>
          logger.error("cache returned error on get", exception)
          None
      }

    private def tryUpdate(key: Key, computed: Value): Value = {
      Try(cache.update(key, computed)) match {
        case Failure(exception) =>
          logger.error("cache returned error on update", exception)
          computed
        case _                  => computed
      }
    }
  }
}
