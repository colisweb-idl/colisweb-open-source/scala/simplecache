package com.colisweb.simplecache.core

trait Cache[Key, Value] {

  def get(key: Key): Option[Value]

  def update(key: Key, value: Value): Unit

  def remove(key: Key): Unit

  def clear(): Unit

}
