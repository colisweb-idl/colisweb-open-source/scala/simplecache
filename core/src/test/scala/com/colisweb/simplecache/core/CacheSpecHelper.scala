package com.colisweb.simplecache.core

import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers._
import org.scalatest.wordspec.AnyWordSpec

trait CacheSpecHelper extends AnyWordSpec with BeforeAndAfterEach {

  def aValidCache[K, V](v1: (K, V), v2: (K, V))(cache: Cache[K, V]): Unit = {
    val (key1, val1) = v1
    val (key2, val2) = v2

    "update and get a value" in {
      cache.clear()

      cache.get(key1) should not be defined
      cache.update(key1, val1)
      cache.get(key1) should contain(val1)
    }

    "update and unset a value" in {
      cache.clear()

      cache.update(key2, val2)
      cache.update(key1, val1)
      cache.remove(key1)
      cache.get(key1) should not be defined
      cache.get(key2) should contain(val2)
    }

    "update a value multiple times" in {
      cache.clear()

      cache.update(key1, val2)
      cache.update(key1, val1)
      cache.get(key1) should contain(val1)
    }

    "set and clear multiple values" in {
      cache.clear()

      cache.update(key1, val1)
      cache.update(key2, val2)

      cache.get(key1) should contain(val1)
      cache.get(key2) should contain(val2)

      cache.clear()

      cache.get(key1) should not be defined
      cache.get(key2) should not be defined
    }

  }
}
