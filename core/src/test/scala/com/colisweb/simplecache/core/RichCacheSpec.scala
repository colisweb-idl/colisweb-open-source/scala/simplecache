package com.colisweb.simplecache.core

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._

import scala.util.{Failure, Success, Try}

class RichCacheSpec extends AnyFlatSpec {

  def aValidRichCache(precision: String, onGet: => Option[String], expected: String): Unit = {
    it should precision in {
      val cache = new Cache[String, String] {
        override def get(key: String): Option[String]         = onGet
        override def update(key: String, value: String): Unit = ()
        override def remove(key: String): Unit                = ()
        override def clear(): Unit                            = ()
      }

      def fastFail: String = throw new Throwable("error")
      val result           = Try(cache.getOrElseUpdate("none", fastFail))
      result match {
        case Failure(exception) => exception.getMessage shouldEqual expected
        case Success(value)     => value shouldEqual expected
      }
    }
  }

  "RichCache" should behave like aValidRichCache(
    precision = "not compute the given value if already set",
    onGet = Some("true"),
    expected = "true"
  )
  "RichCache" should behave like aValidRichCache(
    precision = "compute the given value if not already set",
    onGet = None,
    expected = "error"
  )

  "RichCache" should behave like aValidRichCache(
    precision = "compute the given value if cache is not accessible",
    onGet = throw new Throwable("failing get"),
    expected = "error"
  )
}
