package com.colisweb.simplecache.wrapper

import _root_.cats.MonadError
import _root_.cats.implicits._
import com.colisweb.simplecache.wrapper.error.TaskError
import org.slf4j.LoggerFactory

package object cats {

  implicit class CatsRichCache[F[_], Key, Value](cache: CatsCache[F, Key, Value])(implicit F: MonadError[F, Throwable]) {
    private val logger = LoggerFactory.getLogger(getClass)

    def getOrElseUpdate(key: Key, task: F[Value]): F[Value] =
      tryGet(key) flatMap {
        case Some(value) => F.pure(value)
        case None        => tryUpdate(key, task)
      }

    private def tryGet(key: Key): F[Option[Value]] =
      cache.get(key).attempt.flatMap {
        case Right(cachedOrNone) => F.pure(cachedOrNone)
        case Left(exception)     => F.pure(logger.error("cache returned error on get", exception)) *> F.pure(None)
      }

    private def tryUpdate(key: Key, task: F[Value]): F[Value] =
      task
        .adaptError(TaskError("The given task failed", _))
        .flatTap(cache.update(key, _).handleError(logger.error("cache returned error on update", _)))

  }
}
