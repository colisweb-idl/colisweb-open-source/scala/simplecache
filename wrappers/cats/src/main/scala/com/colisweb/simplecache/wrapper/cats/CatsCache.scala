package com.colisweb.simplecache.wrapper.cats

import cats.MonadError
import cats.effect.Sync
import cats.implicits._
import com.colisweb.simplecache.core.Cache
import com.colisweb.simplecache.wrapper.error.CacheError

sealed abstract class CatsCache[F[_], Key, Value](cache: Cache[Key, Value])(implicit F: MonadError[F, Throwable]) {

  def wrap[A](value: => A): F[A]

  def get(key: Key): F[Option[Value]] = wrap(cache.get(key)).adaptError(CacheError("could not get value from cache", _))

  def update(key: Key, value: Value): F[Unit] =
    wrap(cache.update(key, value)).adaptError(CacheError("could not update value in cache", _))

  def remove(key: Key): F[Unit] = wrap(cache.remove(key)).adaptError(CacheError("could not remove value from cache", _))

  def clear(): F[Unit] = wrap(cache.clear()).adaptError(CacheError("could not clear cache", _))

}

class AsyncCatsCache[F[_]: Sync, Key, Value](cache: Cache[Key, Value]) extends CatsCache[F, Key, Value](cache) {
  override def wrap[A](value: => A): F[A] = Sync[F].delay(value)
}

class SyncCatsCache[F[_], Key, Value](cache: Cache[Key, Value])(implicit F: MonadError[F, Throwable])
    extends CatsCache[F, Key, Value](cache) {
  override def wrap[A](value: => A): F[A] = MonadError[F, Throwable].catchNonFatal(value)
}

object CatsCache {
  def async[F[_]: Sync, Key, Value](cache: Cache[Key, Value]): AsyncCatsCache[F, Key, Value] =
    new AsyncCatsCache[F, Key, Value](cache)

  def sync[F[_], Key, Value](cache: Cache[Key, Value])(implicit
      F: MonadError[F, Throwable]
  ): SyncCatsCache[F, Key, Value] = new SyncCatsCache[F, Key, Value](cache)
}
