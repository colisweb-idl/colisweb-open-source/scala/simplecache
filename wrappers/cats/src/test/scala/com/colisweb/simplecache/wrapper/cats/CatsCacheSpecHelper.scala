package com.colisweb.simplecache.wrapper.cats

import cats.effect.IO
import com.colisweb.simplecache.core.{Cache, CacheSpecHelper}
import cats.effect.unsafe.implicits.global

trait CatsCacheSpecHelper extends CacheSpecHelper {
  def aValidCatsCache[K, V](v1: (K, V), v2: (K, V))(catsCache: CatsCache[IO, K, V]): Unit = {
    val cache: Cache[K, V] = new Cache[K, V] {

      override def get(key: K): Option[V]         = catsCache.get(key).unsafeRunSync()
      override def update(key: K, value: V): Unit = catsCache.update(key, value).unsafeRunSync()
      override def remove(key: K): Unit           = catsCache.remove(key).unsafeRunSync()
      override def clear(): Unit                  = catsCache.clear().unsafeRunSync()
    }

    aValidCache(v1, v2)(cache)
  }
}
