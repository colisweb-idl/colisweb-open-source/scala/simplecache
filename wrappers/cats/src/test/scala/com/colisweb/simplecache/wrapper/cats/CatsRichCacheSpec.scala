package com.colisweb.simplecache.wrapper.cats

import cats.effect.IO
import com.colisweb.simplecache.core.Cache
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import cats.effect.unsafe.implicits.global

class CatsRichCacheSpec extends AnyFlatSpec {

  def aValidRichCache(precision: String, onGet: => Option[String], expected: String): Unit = {
    it should precision in {
      val cache         = new Cache[String, String] {
        override def get(key: String): Option[String]         = onGet
        override def update(key: String, value: String): Unit = ()
        override def remove(key: String): Unit                = ()
        override def clear(): Unit                            = ()
      }
      val catsRichCache = CatsCache.async[IO, String, String](cache)

      def fastFail = IO.raiseError(new Throwable("error"))
      val result   = catsRichCache.getOrElseUpdate("none", fastFail).attempt
      result.unsafeRunSync() match {
        case Left(exception) => exception.getMessage shouldEqual expected
        case Right(value)    => value shouldEqual expected
      }
    }
  }

  "CatsRichCache" should behave like aValidRichCache(
    precision = "not compute the given value if already set",
    onGet = Some("true"),
    expected = "true"
  )
  "CatsRichCache" should behave like aValidRichCache(
    precision = "compute the given value if not already set",
    onGet = None,
    expected = "The given task failed"
  )

  "CatsRichCache" should behave like aValidRichCache(
    precision = "compute the given value if cache is not accessible",
    onGet = throw new Throwable("failing get"),
    expected = "The given task failed"
  )
}
