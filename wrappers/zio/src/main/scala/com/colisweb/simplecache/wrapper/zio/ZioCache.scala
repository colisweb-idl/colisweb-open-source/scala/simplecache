package com.colisweb.simplecache.wrapper.zio

import com.colisweb.simplecache.core.Cache
import com.colisweb.simplecache.wrapper.error.CacheError
import zio.{IO, ZIO}

class ZioCache[Key, Value](cache: Cache[Key, Value]) {
  def get(key: Key): IO[CacheError, Option[Value]] =
    ZIO.attempt(cache.get(key)).mapError(CacheError("could not get value from cache", _))

  def update(key: Key, value: Value): IO[CacheError, Unit] =
    ZIO.attempt(cache.update(key, value)).mapError(CacheError("could not update value in cache", _))

  def remove(key: Key): IO[CacheError, Unit] =
    ZIO.attempt(cache.remove(key)).mapError(CacheError("could not remove value from cache", _))

  def clear(): IO[CacheError, Unit] = ZIO.attempt(cache.clear()).mapError(CacheError("could not clear cache", _))

}

object ZioCache {
  def apply[Key, Value](cache: Cache[Key, Value]): ZioCache[Key, Value] = new ZioCache(cache)
}
