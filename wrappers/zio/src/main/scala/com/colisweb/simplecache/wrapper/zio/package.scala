package com.colisweb.simplecache.wrapper

import _root_.zio.{IO, Task, UIO, ZIO}
import com.colisweb.simplecache.wrapper.error._
import org.slf4j.LoggerFactory

package object zio {
  implicit class ZioRichCache[Key, Value](cache: ZioCache[Key, Value]) {
    private val logger = LoggerFactory.getLogger(getClass)

    def getOrElseUpdate(key: Key, task: Task[Value]): IO[SimplecacheError, Value] =
      tryGet(key).flatMap {
        case Some(value) => ZIO.succeed(value)
        case None        => tryUpdate(key, task)
      }

    private def tryGet(key: Key): UIO[Option[Value]] = {
      cache.get(key).either.flatMap {
        case Right(value)    => ZIO.succeed(value)
        case Left(exception) =>
          ZIO.succeed(logger.error("cache returned error on get", exception)) *> ZIO.none
      }
    }

    private def tryUpdate(key: Key, task: Task[Value]): IO[SimplecacheError, Value] =
      task
        .mapError(TaskError("The given task failed", _))
        .tap(cache.update(key, _).fold(logger.error("cache returned error on update", _), _ => ()))

  }
}
