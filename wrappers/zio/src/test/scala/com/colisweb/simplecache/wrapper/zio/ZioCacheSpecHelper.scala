package com.colisweb.simplecache.wrapper.zio

import zio.{Unsafe, ZIO}
import com.colisweb.simplecache.core.{Cache, CacheSpecHelper}

trait ZioCacheSpecHelper extends CacheSpecHelper {
  def aValidZioCache[K, V](v1: (K, V), v2: (K, V))(zioCache: ZioCache[K, V]): Unit = {
    val cache: Cache[K, V] = new Cache[K, V] {

      override def get(key: K): Option[V] = zioCache.get(key).unsafeRun()

      override def update(key: K, value: V): Unit = zioCache.update(key, value).unsafeRun()

      override def remove(key: K): Unit = zioCache.remove(key).unsafeRun()

      override def clear(): Unit = zioCache.clear().unsafeRun()
    }

    aValidCache(v1, v2)(cache)
  }

  implicit class UnsafeRunImplicit[E <: Throwable, A](computation: ZIO[Any, E, A]) {
    def unsafeRun(): A = Unsafe.unsafe { implicit unsafe =>
      zio.Runtime.default.unsafe.run(computation).getOrThrowFiberFailure()
    }
  }
}

object ZioCacheSpecHelper extends ZioCacheSpecHelper
