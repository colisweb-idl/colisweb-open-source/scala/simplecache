package com.colisweb.simplecache.wrapper.zio

import com.colisweb.simplecache.core.Cache
import ZioCacheSpecHelper.UnsafeRunImplicit
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers._
import zio.ZIO

class ZioRichCacheSpec extends AnyFlatSpec {

  def aValidRichCache(precision: String, onGet: => Option[String], expected: String): Unit = {
    it should precision in {
      val cache    = new Cache[String, String] {
        override def get(key: String): Option[String]         = onGet
        override def update(key: String, value: String): Unit = ()
        override def remove(key: String): Unit                = ()
        override def clear(): Unit                            = ()
      }
      val zioCache = new ZioCache(cache)

      def fastFail = ZIO.fail(new Throwable("error"))
      val result   = zioCache.getOrElseUpdate("none", fastFail).either
      result.unsafeRun() match {
        case Left(exception) => exception.getMessage shouldEqual expected
        case Right(value)    => value shouldEqual expected
      }
    }
  }

  "ZioRichCache" should behave like aValidRichCache(
    precision = "not compute the given value if already set",
    onGet = Some("true"),
    expected = "true"
  )
  "ZioRichCache" should behave like aValidRichCache(
    precision = "compute the given value if not already set",
    onGet = None,
    expected = "The given task failed"
  )

  "ZioRichCache" should behave like aValidRichCache(
    precision = "compute the given value if cache is not accessible",
    onGet = throw new Throwable("failing get"),
    expected = "The given task failed"
  )
}
