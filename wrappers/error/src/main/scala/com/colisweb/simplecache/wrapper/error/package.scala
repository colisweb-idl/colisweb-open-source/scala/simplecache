package com.colisweb.simplecache.wrapper

package error {
  sealed abstract class SimplecacheError(message: String, cause: Throwable) extends RuntimeException(message, cause)

  /** Raised the underlying implementation (such as Redis) failed */
  final case class CacheError(message: String, cause: Throwable) extends SimplecacheError(message, cause)

  /** Raised when the task itself (computing the value) failed */
  final case class TaskError(message: String, cause: Throwable) extends SimplecacheError(message, cause)
}
