ReleaseSettings.globalReleaseSettings
ReleaseSettings.buildReleaseSettings(
  "Simplecache is a small and minimalist caching library",
  "MIT",
  "http://opensource.org/licenses/MIT",
  "simplecache"
)

ThisBuild / developers := List(
  Developers.colasMombrun,
  Developers.florianDoublet,
  Developers.michelDaviot
)
